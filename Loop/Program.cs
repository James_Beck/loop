﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loop
{
    class Program
    {
        static void Main(string[] args)
        {
            ///Where the variables are stored
            int count = 0;
            var i = 0;
            var user = "0";
            var sentence = "0";

            ///Communicating to the user to prompt for the users name and also explain what the program will do with a personalised message
            Console.WriteLine("Hi there, would you give me your name?");
            user = Console.ReadLine();
            Console.WriteLine($"Thank you for that {user}");
            Console.WriteLine("So that you know, I'm a program that can created repeated lines of the same code");

            ///Now the program prompts the person for the sentence they want to repeat and for how many times they would like it repeated
            Console.WriteLine($"So {user} type in a sentence that you would like to repeat");
            sentence = Console.ReadLine();
            Console.WriteLine("Awesome! and how many times would you like to repeat that sentence?");
            count = int.Parse(Console.ReadLine());

            if (count * 0 != 0)
            {
                Console.WriteLine("That isn't a valid number");
            }
            else
            {
                ///The program shows the number of times that the sentence has been printed
                for (i = 0; i < count; i++)
                {
                    var a = i + 1;
                    Console.WriteLine($"This is the {a} iteration of {sentence}");
                }
            }
            
         }
    }
}
